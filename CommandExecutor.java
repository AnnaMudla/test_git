package com.avid.maasdam.fixture.inewsCommandExecutor;

import com.avid.maasdam.fixture.inewsCommandExecutor.cmd.Command;
import com.avid.maasdam.fixture.inewsCommandExecutor.utils.StopWatchTimer;
import com.jcraft.jsch.JSchException;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Taras.Chervonyi
 * Date: 10/31/11
 * Time: 11:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommandExecutor {

    StopWatchTimer timer = new StopWatchTimer();
    private Ssh ssh;

    public CommandExecutor(Config config){
        ssh = new Ssh(config.getUser(), config.getPass(),
                config.getHost(), Integer.parseInt(config.getPort()));
    }

    public void execute(Command... cmd) throws CommandExecutorException {
        timer.start();
        ssh.connect();
        for(Command c : cmd){c.runCommand(ssh);}
        try {
            ssh.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();
        }
        timer.stop();
        System.out.println("---performance---: total command time execution: " + timer.getElapsedTime());
    }

    public void execute(ArrayList<String> cmd){
        ssh.connect();
        try {
            ssh.executeShellCommand(cmd);
            Thread.sleep(10000);
            ssh.disconnect();
            ssh.shellChannel.disconnect();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (CommandExecutorException e) {
            e.printStackTrace();
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }
}



