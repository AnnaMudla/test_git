package com.avid.maasdam.fixture.inewsCommandExecutor;

import com.jcraft.jsch.*;

import java.io.*;
import java.util.List;
import java.util.Properties;


public class Ssh {

    public String userName;
    public String password;
    public String host;
    public int port;
    Session session;
    Channel shellChannel;

    public Ssh(String userName, String password, String host, int port){
        this.userName = userName;
        this.password = password;
        this.host = host;
        this.port = port;
    }

    public void connect() {
        try{
            JSch jsch = new JSch();

            session = jsch.getSession(userName, host, port);

            session.setPassword(password);

            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
            session.setConfig(config);

            System.out.println("[command exec]: session created");
            session.connect();
            System.out.println("[command exec]: session connected");

        }catch(JSchException e){
            e.printStackTrace();
        }
    }

    public void disconnect() throws JSchException{
        session.disconnect();
        System.out.println("[command exec]: session disconnected");
    }

    public String executeCommand(String command) throws CommandExecutorException {
        ChannelExec channel = null;
        try {
            channel = (ChannelExec) session.openChannel("exec");
        } catch (JSchException e) {
            throw new CommandExecutorException("Failed to open channel", e);
        }
        channel.setCommand(command);
        channel.setInputStream(null);
        channel.setErrStream(System.err);
        channel.setExtOutputStream(System.out, true);
        try {
            channel.connect();
        } catch (JSchException e) {
            throw new CommandExecutorException("Failed to connect", e);
        }

        InputStream in = null;
        try {
            in = channel.getInputStream();
        } catch (IOException e) {
            throw new CommandExecutorException("Failed to get input stream", e);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line;
        StringBuffer sb = new StringBuffer();

        try {
            while ((line = br.readLine()) != null) {
                sb.append(line + '\n');
            }
        } catch (IOException e) {
            throw new CommandExecutorException("Failed to read string buffer", e);
        }
        channel.disconnect();
        return sb.toString();
    }
//    public String executeCommandWithInput(String command, InputStream inputStream) throws CommandExecutorException {
//        ChannelExec channel = null;
//        try {
//            channel = (ChannelExec) session.openChannel("exec");
//        } catch (JSchException e) {
//            throw new CommandExecutorException("Failed to open channel", e);
//        }
//        channel.setCommand(command);
//        channel.setInputStream(inputStream);
//        channel.setErrStream(System.err);
//        channel.setExtOutputStream(System.out, true);
//        try {
//            channel.connect();
//        } catch (JSchException e) {
//            throw new CommandExecutorException("Failed to connect", e);
//        }
//
//        InputStream in = null;
//        try {
//            in = channel.getInputStream();
//        } catch (IOException e) {
//            throw new CommandExecutorException("Failed to get input stream", e);
//        }
//        BufferedReader br = new BufferedReader(new InputStreamReader(in));
//
//        String line;
//        StringBuffer sb = new StringBuffer();
//
//        try {
//            while ((line = br.readLine()) != null) {
//                sb.append(line + '\n');
//            }
//        } catch (IOException e) {
//            throw new CommandExecutorException("Failed to read string buffer", e);
//        }
//        channel.disconnect();
//        return sb.toString();
//    }
    public void copyFileFromStream(String filePath, InputStream inputStream) throws CommandExecutorException {
        Channel     channel;
        ChannelSftp channelSftp;

        try{
            channel = session.openChannel("sftp");
        } catch (JSchException e){
            throw new CommandExecutorException("Failed to open channel", e);
        }
        try{
            channel.connect();
        } catch (JSchException e){
            throw new CommandExecutorException("Failed to connect", e);
        }
        channelSftp = (ChannelSftp)channel;
        byte[] buffer = new byte[1024];
        BufferedOutputStream bos;
        try{
            bos= new BufferedOutputStream(channelSftp.put(filePath));
        } catch (SftpException e){
            throw new CommandExecutorException("Failed to open the destination file", e);
        }
        BufferedInputStream bis = new BufferedInputStream(inputStream);
        int readCount;
        try{
            while( (readCount = bis.read(buffer)) > 0) {
                System.out.println("Writing: " );
                bos.write(buffer, 0, readCount);
            }
        } catch(IOException e){
            throw new CommandExecutorException("File copy failed ", e);
        } finally {
            try{
                bis.close();
                bos.close();
            } catch (IOException e){
                System.err.println("Closing the streams failed. Cause:");
                e.printStackTrace();
            }
        }
    }
    public void executeShellCommand(List<String> command) throws CommandExecutorException {
        PrintStream shellStream = null;

        try{
            shellChannel =  session.openChannel("shell");
            shellChannel.setOutputStream(System.out);
            shellStream = new PrintStream(shellChannel.getOutputStream());
            shellChannel.connect();
        }catch (IOException e) {
            e.printStackTrace();
        }
        catch(JSchException e){
            e.printStackTrace();
        }
        for(String cmd : command){
            shellStream.println(cmd);
            shellStream.flush();
        }
    }
}

