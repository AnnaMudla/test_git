package com.avid.maasdam.fixture.inewsCommandExecutor;

import com.jcraft.jsch.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;



// this class is not ready for usage

public class Utility {

    private Config config = new Config();

     private void writeToFile(String data, String fileName){
        try{
            FileWriter fStream = new FileWriter(fileName);
            BufferedWriter out = new BufferedWriter(fStream);
            out.write(data);
            message("-----File created-----");
            out.close();
            message("-----Stream closed-----");
        } catch(IOException e){
            System.out.println(e);
        }
    }

    private void uploadFileOnFTP(String filePath, String destinationPath){
        Session session = null;
        Channel channel = null;
        try {
            JSch ssh = new JSch();

            session = ssh.getSession(Config.getInstance().getUser(), Config.getInstance().getHost(), Integer.parseInt(Config.getInstance().getPort()));
            session.setPassword(Config.getInstance().getPass());

            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
            session.setConfig(config);

            session.connect();

            channel = session.openChannel("sftp");
            channel.connect();
            message("ftp connection established");
            ChannelSftp ftp = (ChannelSftp) channel;
            ftp.put(filePath, destinationPath);
            message("filed uploaded on ftp server");
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
        finally {
            if (channel != null)
            {
                channel.disconnect();
            }
            if (session != null)
            {
                session.disconnect();
            }
        }
    }

    public static void message(String message){
        System.out.println(message);
    }
}
