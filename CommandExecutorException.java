package com.avid.maasdam.fixture.inewsCommandExecutor;

public class CommandExecutorException extends Exception {
//anna
//
//
    public CommandExecutorException() {
        super();
    }

    public CommandExecutorException(Throwable cause) {
        super(cause);
    }

    public CommandExecutorException(String message, Throwable cause) {
        super(message, cause);
    }

}
