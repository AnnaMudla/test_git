package com.avid.maasdam.fixture.inewsCommandExecutor;

/**
 * Created by IntelliJ IDEA.
 * User: Taras.Chervonyi
 * Date: 10/29/11
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class Config {

    private String host;
    private String user;
    private String pass;
    private String port;
    private String nsml_file_path;
    private String nsml_file_name;
    private String nsml_destination_directory;
    private static Config instance;

    public static Config getInstance(){
        if(instance == null){
            instance = new Config();
        } return instance;
    }

     public void setDestinationDirectory(String destinationDirectory){
        this.nsml_destination_directory = destinationDirectory;
    }

    public String getDestinationDirectory(){
        return this.nsml_destination_directory;
    }

    public void setNsmlFilePath(String path){
        this.nsml_file_path = path;
    }

    public void setNsmlFileName(String fileName){
        this.nsml_file_name = fileName;
    }

    public String getNsmlFilePath(){
        return nsml_file_path + nsml_file_name;
    }

    public void setHost(String host){
        this.host = host;
    }

    public void setUser(String user){
        this.user = user;
    }

    public void setPass(String pass){
        this.pass = pass;
    }

    public void setPort(String port){
        this.port = port;
    }

    public String getHost(){
        return host;
    }

    public String getUser(){
        return user;
    }

    public String getPass(){
        return pass;
    }

    public String getPort(){
        return port;
    }
}
